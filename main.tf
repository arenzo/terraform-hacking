provider null {

}

//Creates 20 null resources for experimentation with modules and variables!

module "first_module_instance" {
    source = "./modules/null_module_1"
    count = 5

    module_input_var1 = "From parent module! -- change"
    module_input_var2 = "Also from parent module!"


}

module "second_module_instance" {
    source = "./modules/null_module_2"
    count = 5
    
    module2_input_var1 = "This is second parent module instance"
    module2_input_var2 = "Also second parent module instance!!!"

}

terraform {
    backend "http" {
        
    }
}

output "module1_output" {
    value = module.first_module_instance
}

output "module2_output" {
    value = module.second_module_instance
}
