resource "null_resource" "application-null-resource1" {

    triggers = {
        mytrigger = local.variable1
    }

}


resource "null_resource" "application-null-resource2" {

    triggers = {
        mytrigger = local.variable2
    }
}


resource "null_resource" "application-null-resource3" {

    triggers = {
        mytrigger = local.name
    }
}

output "fqdn_name" {
    value = local.name
}