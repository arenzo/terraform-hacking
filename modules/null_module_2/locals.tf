locals {
    variable1 = var.module2_input_var1
    variable2 = var.module2_input_var2
}

//another locals block

locals {

    terraform_workspace = "Currently in TF Workspace: ${terraform.workspace}"
}